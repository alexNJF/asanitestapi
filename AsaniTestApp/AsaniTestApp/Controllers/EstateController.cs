﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Security.Claims;
using DAL.DTO;
using Microsoft.Extensions.Logging;

namespace AsaniTestApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EstateController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EstateController> _logger;


        public EstateController(ILogger<EstateController> logger, IUnitOfWork unitOfWork )
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
      
        }

        [HttpGet("List")]
        [Authorize]
        public async Task<IActionResult> List()
        {
          
            try
            { 
                var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
                var result = _unitOfWork.EstateService.GetEstates(userId,HttpContext);
                return Ok(result);

            }

            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost("AddOrUpdate")]
        [Authorize]
        public async Task<IActionResult> AddOrUpdate(EstateDto estate)
        {
            try
            {
                var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
                var result = await _unitOfWork.EstateService.CreateOrUpdate(estate, userId, HttpContext);
                return Ok(new { sucsess = result.Item1, message = result.Item2 });

            }

            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        #region Delete
        [HttpDelete, Route("Delete/{Id}")]
        [Authorize]

        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                var userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
                var result = await _unitOfWork.EstateService.Delete(userId,Id, HttpContext);
                return Ok(new {sucsess= result.Item1,message=result.Item2 });

            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion

       

    }
}
