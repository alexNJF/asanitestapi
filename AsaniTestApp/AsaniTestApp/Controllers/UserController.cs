﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Domain;
using DAL.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace AsaniTestApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IJWTService _jwtAuthenticationHandler;
       

        public UserController(IUnitOfWork unitOfWork, IJWTService jwtAuthenticationHandler)
        {
            _unitOfWork = unitOfWork;
            _jwtAuthenticationHandler = jwtAuthenticationHandler;
            
        }


        #region Login
        [HttpPost, Route("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginInformationDTO user)
        {
            try
            {
                if (user == null) return BadRequest("NULL Information");
                var foundUser = _unitOfWork.UserService.GetUserByUserPassword(user);
                if (foundUser != null)
                {
                    var res = await DoLogin(foundUser);

                    if (res.Item1)
                    {
                        return Ok(new { token = res.Item2, logged_in = true });
                    }
                }

                return BadRequest(new { logged_in = false, message = "نام کاربری یا کلمه عبور اشتباه است" });
            }
            catch (Exception e)
            {
                return  BadRequest(e);
            }
        }
        #endregion

        #region DoLogin
        private async Task<(bool, string)> DoLogin(User foundUser)
        {

            var jwt = _jwtAuthenticationHandler.Create(foundUser);
            return (true, jwt);
           
        }
        #endregion

       


    }
}
