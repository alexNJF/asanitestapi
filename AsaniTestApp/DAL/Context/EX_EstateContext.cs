﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Context
{
    public partial class EstateContext : DbContext
    {
        public static string connectionString { get; set; }
        public EstateContext(string ConnectionString)
        {
            if (!string.IsNullOrEmpty(ConnectionString))
                connectionString = ConnectionString;
        }

    }
}
