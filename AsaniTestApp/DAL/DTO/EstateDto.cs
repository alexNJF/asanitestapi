﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTO
{
    public class EstateDto
    {
        public int? Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public bool IsNorth { get; set; }
       

    }
}
