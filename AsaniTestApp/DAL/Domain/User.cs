﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Domain
{
    public partial class User
    {
        public User()
        {
            Estates = new HashSet<Estate>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public bool IsDelete { get; set; }

        public virtual ICollection<Estate> Estates { get; set; }
    }
}
