﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Domain
{
    public partial class Estate
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public bool IsNorth { get; set; }
        public bool IsDelete { get; set; }

        public virtual User User { get; set; }
    }
}
