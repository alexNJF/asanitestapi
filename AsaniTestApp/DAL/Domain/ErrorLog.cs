﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Domain
{
    public partial class ErrorLog
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public DateTime? LogDateTime { get; set; }
        public string Address { get; set; }
        public string Exception { get; set; }
        public string UserIp { get; set; }
        public string InputData { get; set; }
    }
}
