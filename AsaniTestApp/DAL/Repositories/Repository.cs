﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _applicatiopnContext;

        private readonly DbSet<T> Local;
        readonly string errorMessage = string.Empty;

        protected Repository(DbContext applicatiopnContext)
        {
            _applicatiopnContext = applicatiopnContext;
            Local = applicatiopnContext.Set<T>();
        }

        public virtual IQueryable<T> FindAll(Expression<Func<T, bool>> where = null)
        {
            IQueryable<T> data = Local;

            if (where != null)
                data = data.Where(where);
            return data;
        }


        public virtual T Find(object id)
        {
            return Local.Find(id);
        }

        public virtual void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            Local.Add(entity);
        }

        public virtual async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            await Local.AddAsync(entity);
        }

        public virtual void Update(T entity)
        {
            _applicatiopnContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            Local.Remove(entity);
        }

        public virtual void Delete(object id)
        {
            Delete(Find(id));
        }

        public virtual async Task InsertRangeAsync(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entity");
            }
            await Local.AddRangeAsync(entities);
        }

        public virtual T FindFirst(Expression<Func<T, bool>> where = null)
        {
            return FindAll(where).First();
        }

    }
}
