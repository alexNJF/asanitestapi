﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IRepository<T> where T : class
    {
        void Delete(object id);
        void Delete(T entity);
        T Find(object id);
        IQueryable<T> FindAll(Expression<Func<T, bool>> where = null);
        T FindFirst(Expression<Func<T, bool>> where = null);
        void Insert(T entity);
        Task InsertAsync(T entity);
        Task InsertRangeAsync(IEnumerable<T> entities);
        void Update(T entity);
    }
}