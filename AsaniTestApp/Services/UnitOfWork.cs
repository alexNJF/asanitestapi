﻿using DAL.Context;
using DAL.Domain;
using DAL.Repositories;
using Microsoft.Extensions.Configuration;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EstateContext _context;
        private readonly IConfiguration _configuration;

        public UnitOfWork(EstateContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        #region SaveChanges
        public void SaveChanges()
        {

            _context.SaveChanges();
        }
        #endregion
        #region SaveChangesAsync
        public async Task SaveChangesAsync()
        {

            await _context.SaveChangesAsync();
        }
        #endregion
        #region GetContext
        public EstateContext GetContext()
        {
            return _context;
        }
        #endregion

        #region User
        private IRepository<User> _User;

        public IRepository<User> User
        {
            get { return _User = _User ?? new UserService(_context, this); }
        }
        public UserService UserService => (UserService)User;
        #endregion

        #region Estate
        private IRepository<Estate> _Estate;

        public IRepository<Estate> Estate
        {
            get { return _Estate = _Estate ?? new EstateService(_context, this); }
        }
        public EstateService EstateService => (EstateService)Estate;
        #endregion

        #region ErrorLog
        private IRepository<ErrorLog> _ErrorLog;

        public IRepository<ErrorLog> ErrorLog
        {
            get { return _ErrorLog = _ErrorLog ?? new LoggerService(_context, this); }
        }
        public LoggerService ErrorLogService => (LoggerService)ErrorLog;
        #endregion



    }
}
