﻿using DAL.Domain;

namespace Services
{
    public interface IJWTService
    {
        string Create(User user);
    }
}