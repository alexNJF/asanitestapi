﻿using DAL.Domain;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services
{
    public class JWTService : IJWTService
    {
        public string Create(User user)
        {
            var SecretKey = Encoding.UTF8.GetBytes("ASANI_SECRET_KEY");
            var SigingCredential = new SigningCredentials(new SymmetricSecurityKey(SecretKey), SecurityAlgorithms.HmacSha256);
            var Claimes = _getClaime(user);
            var discriptor = new SecurityTokenDescriptor
            {
                Issuer = "ASANI_TEST",
                Audience = "ASANI_TEST",
                IssuedAt = DateTime.Now,
                NotBefore=DateTime.Now.AddMinutes(0),
                Expires = DateTime.Now.AddHours(1),
                SigningCredentials = SigingCredential,
                Subject = new ClaimsIdentity(_getClaime(user))
            };
            var TokenHandler = new JwtSecurityTokenHandler();
            var SecurtyToken = TokenHandler.CreateToken(discriptor);
            var jwt = TokenHandler.WriteToken(SecurtyToken);
            return jwt;
        }
        private IEnumerable<Claim> _getClaime(User user)
        {
            var List = new List<Claim>
                {
                new Claim(ClaimTypes.Name, user.FirstName + user.LastName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };
            return List;
        }
    }
}
