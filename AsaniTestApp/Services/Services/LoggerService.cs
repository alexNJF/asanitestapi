﻿using DAL.Context;
using DAL.Domain;
using DAL.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Services.Services
{
    public class LoggerService : Repository<ErrorLog>
    {
        private readonly IUnitOfWork _unitOfWork;

        #region LoggerService
        public LoggerService(EstateContext context, IUnitOfWork unitOfWork) : base(context)
        {
            _unitOfWork = unitOfWork;


        }
        #endregion

        public void CreateLog(int userid, HttpContext httpContext)
        {

          

            var log = new ErrorLog
            {
                UserId = userid,
                UserIp = httpContext.Connection.RemoteIpAddress.ToString()== "::1"? System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString() : httpContext.Connection.RemoteIpAddress.ToString(),
                LogDateTime = DateTime.Now,
                //InputData=_getRequestBody(httpContext.Request.Body),
                Address = httpContext.Request.Path
                
            };
            Insert(log);
            _unitOfWork.SaveChanges();
        }

        private  string _getRequestBody(Stream stream)
        {
            var bodyString = "";
            try
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8, true, 1024, true))
                {
                    bodyString = reader.ReadToEnd();
                }
                stream.Position = 0;
                return bodyString;

            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
}
