﻿using DAL.Context;
using DAL.Domain;
using DAL.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using DAL.DTO;
using System.Linq;

namespace Services.Services
{
    public class UserService:Repository<User>
    {
        private readonly IUnitOfWork _unitOfWork;

        #region UserService
        public UserService(EstateContext context, IUnitOfWork unitOfWork) : base(context)
        {
            _unitOfWork = unitOfWork;
            
          
        }
        #endregion
        #region GetUserById
        public User GetUserById(int id)
        {
            var result = Find(new User { Id = id });
            return result;

        }

        #endregion

        public User GetUserByUserPassword(LoginInformationDTO loginInformation )
        {
           var  passwordHashbyte = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(loginInformation.Password)) ;
            var passswordHashString = Convert.ToBase64String(passwordHashbyte);
          
            var result = FindAll(x => x.UserName == loginInformation.Username &&x.PasswordHash==passswordHashString).FirstOrDefault();
            return result;

        }
    }
}
