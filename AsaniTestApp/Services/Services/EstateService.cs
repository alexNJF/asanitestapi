﻿using DAL.Context;
using DAL.Domain;
using DAL.DTO;
using DAL.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class EstateService:Repository<Estate>
    {
        private readonly IUnitOfWork _unitOfWork;

        #region EstateService
        public EstateService(EstateContext context, IUnitOfWork unitOfWork) : base(context)
        {
            _unitOfWork = unitOfWork;


        }
        #endregion
        /// <summary>
        /// </summary>
        /// <returns>
        /// مجموعه  املاک کاربر جاری را برمیگرداند 
        /// </returns>
       public IEnumerable<EstateDto> GetEstates(int userId, HttpContext httpContext)
        {
            var result = FindAll(x => x.UserId == userId && x.IsDelete == false)
                .Select(x=>new EstateDto 
                {
                    Id=x.Id,
                    UserId=x.UserId,
                    Name=x.Name,
                    Area=x.Area,
                    Address=x.Address,
                    IsNorth=x.IsNorth
                }).ToList() ;
            _unitOfWork.ErrorLogService.CreateLog(userId, httpContext);
            return result;
        }



        /// <summary>
        /// ایجاد یا ویرایش ملک جدید 
        /// </summary>
        /// <param name="estate">
        /// مدلی که از کاربر جهت ایجاد یا ویرایش ملک جدید دریافت کرده ایم 
        /// </param>
        public async Task<(bool, string)> CreateOrUpdate (EstateDto estate ,int userId, HttpContext httpContext)
        {
            var newEstate = new Estate
            {
                Id = estate.Id ?? 0,
                UserId = userId,
                Name = estate.Name,
                Address = estate.Address,
                Area = estate.Area,
                IsNorth = estate.IsNorth,
                IsDelete = false
                
            };
            if(string.IsNullOrEmpty(estate.Id.ToString()))

                _unitOfWork.Estate.Insert(newEstate);
            else
                _unitOfWork.Estate.Update(newEstate);

            _unitOfWork.ErrorLogService.CreateLog(userId, httpContext);
            await _unitOfWork.SaveChangesAsync();
            return (true, "Action Sucsess ");

        }

      

        #region Delete
        /// <summary>
        /// ایتم مورد نظر را یافت میکند و فیلد 
        /// isDelete
        /// آن را 
        /// true 
        /// میکند 
        /// </summary>
        /// <param name="Eestateid">
        /// ای دی ایتمی که بایست پاک شود 
        /// </param>
        /// <returns></returns>
        public async Task<(bool, string)> Delete(int userId,int Eestateid, HttpContext httpContext)
        {
            try
            {
                var item = Find(Eestateid);
                if (item == null || item.IsDelete)
                    return (false,"Item Not Found");
                item.IsDelete = true;
                Update(item);
                _unitOfWork.ErrorLogService.CreateLog(userId, httpContext);
                await _unitOfWork.SaveChangesAsync();

                return (true, "successfully Deleted ");
            }
            catch (Exception ex)
            {
                return (false, ex.Message);
            }
        }
        #endregion





    }
}
