﻿using DAL.Context;
using DAL.Domain;
using DAL.Repositories;
using Services.Services;
using System.Threading.Tasks;

namespace Services
{
    public interface IUnitOfWork
    {
        IRepository<User> User { get; }
        UserService UserService { get; }
        EstateService EstateService { get; }
        IRepository<Estate> Estate { get; }
        LoggerService ErrorLogService { get; }
        IRepository<ErrorLog> ErrorLog { get; }

        EstateContext GetContext();
        void SaveChanges();
        Task SaveChangesAsync();
    }
}