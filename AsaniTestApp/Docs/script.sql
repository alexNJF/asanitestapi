USE [master]
GO
/****** Object:  Database [Estate]    Script Date: 12/11/2020 4:17:42 PM ******/
CREATE DATABASE [Estate]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Estate', FILENAME = N'D:\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Estate.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Estate_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Estate_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Estate] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Estate].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Estate] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Estate] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Estate] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Estate] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Estate] SET ARITHABORT OFF 
GO
ALTER DATABASE [Estate] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Estate] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Estate] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Estate] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Estate] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Estate] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Estate] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Estate] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Estate] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Estate] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Estate] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Estate] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Estate] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Estate] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Estate] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Estate] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Estate] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Estate] SET RECOVERY FULL 
GO
ALTER DATABASE [Estate] SET  MULTI_USER 
GO
ALTER DATABASE [Estate] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Estate] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Estate] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Estate] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Estate] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Estate', N'ON'
GO
ALTER DATABASE [Estate] SET QUERY_STORE = OFF
GO
USE [Estate]
GO
/****** Object:  Schema [LOG]    Script Date: 12/11/2020 4:17:43 PM ******/
CREATE SCHEMA [LOG]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 12/11/2020 4:17:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estate]    Script Date: 12/11/2020 4:17:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Area] [nvarchar](50) NULL,
	[Address] [nvarchar](max) NOT NULL,
	[IsNorth] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Estate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/11/2020 4:17:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](11) NULL,
	[UserName] [nvarchar](max) NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [LOG].[ErrorLog]    Script Date: 12/11/2020 4:17:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [LOG].[ErrorLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[LogDateTime] [datetime] NULL,
	[Address] [nvarchar](128) NULL,
	[Exception] [nvarchar](max) NULL,
	[UserIP] [varchar](50) NULL,
	[InputData] [nvarchar](max) NULL,
 CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201210084817_initail Magrtion ', N'5.0.0')
SET IDENTITY_INSERT [dbo].[Estate] ON 

INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (1, 3, N'خانه اول ', N'8000', N'ایران ', 0, 0)
INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (2, 3, N'خانه دوم', N'80', N'ایران ', 1, 0)
INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (3, 3, N'خانه سوم ', N'150', N'ایران', 0, 1)
INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (5, 3, N'خانه هفتم ', N'8000', N'آمریکا ', 0, 1)
INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (6, 3, N'خانه هفتم ', N'8000', N'آمریکا ', 0, 1)
INSERT [dbo].[Estate] ([Id], [UserId], [Name], [Area], [Address], [IsNorth], [IsDelete]) VALUES (7, 3, N'خانه هفتم ', N'8000', N'آمریکا ', 0, 1)
SET IDENTITY_INSERT [dbo].[Estate] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [FirstName], [LastName], [Phone], [UserName], [PasswordHash], [IsDelete]) VALUES (3, N'ali', N'hakimi', N'09393496846', N'ali', N'jZae727K08KaOmKSgOaGzww/XVqGr/PKEgIMkjrcbJI=', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [LOG].[ErrorLog] ON 

INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (1, 3, CAST(N'2020-12-11T14:45:54.297' AS DateTime), N'/Estate/List', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (2, 3, CAST(N'2020-12-11T15:38:30.830' AS DateTime), N'/Estate/List', NULL, N'::1', N'')
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (3, 3, CAST(N'2020-12-11T15:44:09.487' AS DateTime), N'/Estate/List', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (4, 3, CAST(N'2020-12-11T15:44:38.647' AS DateTime), N'/Estate/AddOrUpdate', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (5, 3, CAST(N'2020-12-11T15:45:42.457' AS DateTime), N'/Estate/List', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (6, 3, CAST(N'2020-12-11T15:46:07.613' AS DateTime), N'/Estate/Delete/6', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (7, 3, CAST(N'2020-12-11T15:46:13.590' AS DateTime), N'/Estate/List', NULL, N'::1', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (8, 3, CAST(N'2020-12-11T15:49:54.590' AS DateTime), N'/Estate/List', NULL, N'fe80::6832:4ea6:18a5:ecdb%19', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (9, 3, CAST(N'2020-12-11T16:09:56.507' AS DateTime), N'/Estate/AddOrUpdate', NULL, N'fe80::6832:4ea6:18a5:ecdb%19', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (10, 3, CAST(N'2020-12-11T16:10:07.917' AS DateTime), N'/Estate/List', NULL, N'fe80::6832:4ea6:18a5:ecdb%19', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (11, 3, CAST(N'2020-12-11T16:10:34.320' AS DateTime), N'/Estate/Delete/7', NULL, N'fe80::6832:4ea6:18a5:ecdb%19', NULL)
INSERT [LOG].[ErrorLog] ([Id], [UserId], [LogDateTime], [Address], [Exception], [UserIP], [InputData]) VALUES (12, 3, CAST(N'2020-12-11T16:10:38.830' AS DateTime), N'/Estate/List', NULL, N'fe80::6832:4ea6:18a5:ecdb%19', NULL)
SET IDENTITY_INSERT [LOG].[ErrorLog] OFF
/****** Object:  Index [IX_Estate_UserId]    Script Date: 12/11/2020 4:17:43 PM ******/
CREATE NONCLUSTERED INDEX [IX_Estate_UserId] ON [dbo].[Estate]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Estate]  WITH CHECK ADD  CONSTRAINT [FK_Estate_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Estate] CHECK CONSTRAINT [FK_Estate_Users]
GO
USE [master]
GO
ALTER DATABASE [Estate] SET  READ_WRITE 
GO
